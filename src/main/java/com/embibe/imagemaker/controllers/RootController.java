package com.embibe.imagemaker.controllers;

import com.embibe.imagemaker.models.MathRequest;
import com.embibe.imagemaker.models.ResponseData;
import com.embibe.imagemaker.services.ImageMakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
public class RootController {

    @Autowired
    ImageMakerService imageMakerService;


    @RequestMapping("/convert")
    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> loginUser(@RequestHeader Map<String, String> headers,
                                            @RequestBody MathRequest request) throws IOException {
        ResponseData resp = imageMakerService.generate(request);
        return ResponseEntity.ok().body(resp);
    }
}
