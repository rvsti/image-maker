package com.embibe.imagemaker.services;

import com.embibe.imagemaker.models.MathRequest;
import com.embibe.imagemaker.models.ResponseData;
import com.embibe.imagemaker.utils.MimeType;
import net.sourceforge.jeuclid.context.LayoutContextImpl;
import net.sourceforge.jeuclid.parser.Parser;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;

@Service
public class ImageMakerService implements DiagramGenerator
{

    @Override
    public ResponseData generate(MathRequest request) throws IOException
    {
        Converter converter;
        MimeType format = null;
        if(request.getType().equals("svg")) {
            format = MimeType.SVG;
            converter = new SVGConverter();
        }
        else if(request.getType().equals("png")) {
            format = MimeType.PNG;
            converter = new PNGConverter();
        }
        else
            throw new IOException("Unsupported mime type format");

        String mathml = request.getData();

        Document mathMlDocument;
        try {
            mathMlDocument = Parser.getInstance().parseStreamSource(new StreamSource(new StringReader(mathml)));
        } catch (SAXException e) {
            throw new IOException(e);
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        converter.convert(mathMlDocument, new LayoutContextImpl(LayoutContextImpl.getDefaultLayoutContext()), out);
        out.close();

        return new ResponseData(format, out.toByteArray());
    }

}

