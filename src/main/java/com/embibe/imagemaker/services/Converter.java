package com.embibe.imagemaker.services;

import net.sourceforge.jeuclid.LayoutContext;
import org.w3c.dom.Node;

import java.io.IOException;
import java.io.OutputStream;

public interface Converter {
    void convert(Node node, LayoutContext ctx, OutputStream out) throws IOException;
}

