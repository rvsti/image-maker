package com.embibe.imagemaker.services;

import net.sourceforge.jeuclid.LayoutContext;
import net.sourceforge.jeuclid.layout.JEuclidView;
import org.apache.batik.svggen.SVGGeneratorContext;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.io.IOException;
import java.io.OutputStream;

public class SVGConverter implements Converter {
    public void convert(Node node, LayoutContext ctx, OutputStream out) throws IOException
    {
        Document document;
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        } catch (ParserConfigurationException e1) {
            throw new IOException(e1);
        }
        document.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "svg"));

        SVGGeneratorContext svgCtx = SVGGeneratorContext.createDefault(document);
        svgCtx.setComment("Converted from MathML using JEuclid");

        SVGGraphics2D graphics = new SVGGraphics2D(svgCtx, true);

        JEuclidView view = new JEuclidView(node, ctx, graphics);
        int ascentHeight = (int) Math.ceil((double) view.getAscentHeight());
        int descentHeight = (int) Math.ceil((double) view.getDescentHeight());
        int height = ascentHeight + descentHeight;
        int width = (int) Math.ceil((double) view.getWidth());
        graphics.setSVGCanvasSize(new Dimension(width, height));
        view.draw(graphics, 0.0F, (float) ascentHeight);
        document.replaceChild(graphics.getRoot(), document.getFirstChild());

        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(out);
            transformer.transform(source, result);
        } catch (TransformerException e) {
            throw new IOException(e);
        }
    }
}
