package com.embibe.imagemaker.services;

import com.embibe.imagemaker.utils.ImageWriterUtil;
import com.embibe.imagemaker.utils.MimeType;
import net.sourceforge.jeuclid.LayoutContext;
import net.sourceforge.jeuclid.layout.JEuclidView;
import org.w3c.dom.Node;

import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

public class PNGConverter implements Converter {
    public void convert(Node node, LayoutContext ctx, OutputStream out) throws IOException {
        BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = image.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        JEuclidView view = new JEuclidView(node, ctx, graphics);
        int ascentHeight = (int) Math.ceil((double) view.getAscentHeight());
        int descentHeight = (int) Math.ceil((double) view.getDescentHeight());
        int height = ascentHeight + descentHeight;
        int width = (int) Math.ceil((double) view.getWidth());

        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        graphics = image.createGraphics();
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        view.draw(graphics, 0.0F, (float) ascentHeight);

        ImageOutputStream imageOutputStream = new MemoryCacheImageOutputStream(out);

        ImageWriter writer = ImageWriterUtil.getImageWriter(MimeType.PNG);
        writer.setOutput(imageOutputStream);
        writer.write(image);
        imageOutputStream.close();
    }
}
