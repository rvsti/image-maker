package com.embibe.imagemaker.services;

import com.embibe.imagemaker.models.MathRequest;
import com.embibe.imagemaker.models.ResponseData;

import java.io.IOException;

public interface DiagramGenerator {

    ResponseData generate(MathRequest request) throws IOException;
}
