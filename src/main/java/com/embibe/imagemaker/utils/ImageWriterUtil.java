package com.embibe.imagemaker.utils;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.util.Iterator;

public class ImageWriterUtil {

    public static javax.imageio.ImageWriter getImageWriter(MimeType mimeType) throws IOException
    {
        Iterator<javax.imageio.ImageWriter> writers = ImageIO.getImageWritersByMIMEType(mimeType.toString());
        if (!writers.hasNext()) {
            throw new IOException("Could not find ImageIO writer for " + mimeType);
        }
        return writers.next();
    }
}
