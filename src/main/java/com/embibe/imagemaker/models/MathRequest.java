package com.embibe.imagemaker.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MathRequest {
    @JsonProperty("data")
    private String data;
    @JsonProperty("type")
    private String type;
}
